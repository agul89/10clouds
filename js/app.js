(function () {
    var app = angular.module('app', ['ngRoute']);

    app.config(function($routeProvider) {
        $routeProvider

            .when('/', {
                templateUrl : './views/home.html',
                controller  : 'HomeController'
            })

            .when('/login', {
                templateUrl : './views/login.html',
                controller  : 'LoginController',
            })

            .when('/table', {
                templateUrl : './views/table.html',
                controller  : 'TableController',
            })
    });

    app.controller('AppController', function ($scope, $location, $rootScope, $timeout) {
        $rootScope.$on('$routeChangeStart', function() {
            if (!$scope.isLogged()) {
                $location.path( "/login" );
            }
        });

        $scope.isLogged = function () {
            return sessionStorage.getItem('loginData') !== null;
        };

        $scope.logout = function () {
            sessionStorage.removeItem('loginData');
            $location.path( "/login" );
        };

        $scope.alerts = [];

        $rootScope.showSuccessAlert = function () {
            $scope.alerts = [{
                type: 'success'
            }];

            $timeout($rootScope.dismissAlert, 1500);
        };

        $rootScope.dismissAlert = function () {
            $scope.alerts = [];
        };
    });

    app.controller('HomeController', function($scope, $rootScope) {
        $scope.getUserInput = function (event, userInput) {
            event.preventDefault();

            var entryData = [],
                newEntry = {};

            if (sessionStorage.getItem('entryData') !== null) {
                entryData = JSON.parse(sessionStorage.getItem('entryData'));
            }

            newEntry.hash = $scope.hashString(userInput);
            newEntry.input = userInput;
            newEntry.creationDate = Date.now();
            newEntry.userEmail = sessionStorage.getItem('loginData');

            entryData.push(newEntry);

            sessionStorage.setItem('entryData', JSON.stringify(entryData));

            $rootScope.showSuccessAlert();
        };

        //This hashing function might not be the most optimal, but I somehow managed to make it work...
        //Original snippet found here: https://css-tricks.com/snippets/javascript/javascript-md5/

        $scope.hashString = function (string) {
            function RotateLeft(lValue, iShiftBits) {
                return (lValue<<iShiftBits) | (lValue>>>(32-iShiftBits));
            }

            function AddUnsigned(lX,lY) {
                var lX4,lY4,lX8,lY8,lResult;
                lX8 = (lX & 0x80000000);
                lY8 = (lY & 0x80000000);
                lX4 = (lX & 0x40000000);
                lY4 = (lY & 0x40000000);
                lResult = (lX & 0x3FFFFFFF)+(lY & 0x3FFFFFFF);
                if (lX4 & lY4) {
                    return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
                }
                if (lX4 | lY4) {
                    if (lResult & 0x40000000) {
                        return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
                    } else {
                        return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
                    }
                } else {
                    return (lResult ^ lX8 ^ lY8);
                }
            }

            function F(x,y,z) { return (x & y) | ((~x) & z); }
            function G(x,y,z) { return (x & z) | (y & (~z)); }
            function H(x,y,z) { return (x ^ y ^ z); }
            function I(x,y,z) { return (y ^ (x | (~z))); }

            function FF(a,b,c,d,x,s,ac) {
                a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
                return AddUnsigned(RotateLeft(a, s), b);
            }

            function GG(a,b,c,d,x,s,ac) {
                a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
                return AddUnsigned(RotateLeft(a, s), b);
            }

            function HH(a,b,c,d,x,s,ac) {
                a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
                return AddUnsigned(RotateLeft(a, s), b);
            }

            function II(a,b,c,d,x,s,ac) {
                a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
                return AddUnsigned(RotateLeft(a, s), b);
            }

            function ConvertToWordArray(string) {
                var lWordCount;
                var lMessageLength = string.length;
                var lNumberOfWords_temp1=lMessageLength + 8;
                var lNumberOfWords_temp2=(lNumberOfWords_temp1-(lNumberOfWords_temp1 % 64))/64;
                var lNumberOfWords = (lNumberOfWords_temp2+1)*16;
                var lWordArray=Array(lNumberOfWords-1);
                var lBytePosition = 0;
                var lByteCount = 0;
                while ( lByteCount < lMessageLength ) {
                    lWordCount = (lByteCount-(lByteCount % 4))/4;
                    lBytePosition = (lByteCount % 4)*8;
                    lWordArray[lWordCount] = (lWordArray[lWordCount] | (string.charCodeAt(lByteCount)<<lBytePosition));
                    lByteCount++;
                }
                lWordCount = (lByteCount-(lByteCount % 4))/4;
                lBytePosition = (lByteCount % 4)*8;
                lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80<<lBytePosition);
                lWordArray[lNumberOfWords-2] = lMessageLength<<3;
                lWordArray[lNumberOfWords-1] = lMessageLength>>>29;
                return lWordArray;
            }

            function WordToHex(lValue) {
                var WordToHexValue="",WordToHexValue_temp="",lByte,lCount;
                for (lCount = 0;lCount<=3;lCount++) {
                    lByte = (lValue>>>(lCount*8)) & 255;
                    WordToHexValue_temp = "0" + lByte.toString(16);
                    WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length-2,2);
                }
                return WordToHexValue;
            }

            function Utf8Encode(string) {
                string = string.replace(/\r\n/g,"\n");
                var utftext = "";

                for (var n = 0; n < string.length; n++) {

                    var c = string.charCodeAt(n);

                    if (c < 128) {
                        utftext += String.fromCharCode(c);
                    }
                    else if((c > 127) && (c < 2048)) {
                        utftext += String.fromCharCode((c >> 6) | 192);
                        utftext += String.fromCharCode((c & 63) | 128);
                    }
                    else {
                        utftext += String.fromCharCode((c >> 12) | 224);
                        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                        utftext += String.fromCharCode((c & 63) | 128);
                    }

                }

                return utftext;
            }

            var x = Array();
            var k,AA,a;
            var S11=7;
            var S21=5;
            var S31=4;
            var S41=6;

            string = Utf8Encode(string);

            x = ConvertToWordArray(string);

            a = 0x67452301;

            for (k=0;k<x.length;k+=16) {
                AA=a;
                a=FF(a,x[k+0], S11,0xD76AA478);
                a=FF(a,x[k+4], S11,0xF57C0FAF);
                a=FF(a,x[k+8], S11,0x698098D8);
                a=FF(a,x[k+12],S11,0x6B901122);
                a=GG(a,x[k+1], S21,0xF61E2562);
                a=GG(a,x[k+5], S21,0xD62F105D);
                a=GG(a,x[k+9], S21,0x21E1CDE6);
                a=GG(a,x[k+13],S21,0xA9E3E905);
                a=HH(a,x[k+5], S31,0xFFFA3942);
                a=HH(a,x[k+1], S31,0xA4BEEA44);
                a=HH(a,x[k+13],S31,0x289B7EC6);
                a=HH(a,x[k+9], S31,0xD9D4D039);
                a=II(a,x[k+0], S41,0xF4292244);
                a=II(a,x[k+12],S41,0x655B59C3);
                a=II(a,x[k+8], S41,0x6FA87E4F);
                a=II(a,x[k+4], S41,0xF7537E82);
                a=AddUnsigned(a,AA);
            }

            var temp = WordToHex(a);

            return temp.toLowerCase();
        };
    });

    app.controller('TableController', function($scope) {
        $scope.Entries = JSON.parse(sessionStorage.getItem('entryData'));
    });

    app.controller('LoginController', function($scope, $location) {
        $scope.login = function (event, loginData) {
            event.preventDefault();
            sessionStorage.setItem('loginData', loginData);

            $location.path( "/" );
        };
    });

})();

